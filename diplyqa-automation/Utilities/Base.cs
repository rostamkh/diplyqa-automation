﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Safari;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace diplyqa_automation.Utilities
{
    [SetUpFixture]
    class Base
    {
        public IWebDriver driver;

        public static ExtentReports extent;
        public static ExtentHtmlReporter htmlReporter;
        public static ExtentTest test;

        [OneTimeSetUp]
        public void SetupReporting()
        {
            string fileName = "QAAutoResult-" + DateForName() + ".html";
            string ReportFolderPath = $@"{AppDomain.CurrentDomain.BaseDirectory}..\..\Reports\{fileName}";

            htmlReporter = new ExtentHtmlReporter(ReportFolderPath);
            htmlReporter.Configuration().Theme = Theme.Dark;
            htmlReporter.Configuration().DocumentTitle = "Diply QA Automation Test";
            htmlReporter.Configuration().ReportName = "Diply - QA Automation Test";

            htmlReporter.Configuration().JS = "$('.brand-logo').text('').append('<img src=C:\\Users\\RostamKhosraviani\\Pictures\\diply-logo-2.png>')";
            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);

        }

        [OneTimeTearDown]
        public void GenerateReport()
        {
            extent.Flush();
        }


        public void SelectDriverBrowser(string browser)
        {

            if (browser.ToLower() == "chrome")
            {
                driver = new ChromeDriver();
            }
            else
            {
                if (browser.ToLower() == "firefox")
                {
                    driver = new FirefoxDriver();
                    //driver = new GeckoDriver();
                }
                else
                {
                    if (browser.ToLower() == "edge")
                    {
                        driver = new EdgeDriver();
                    }
                    else
                    {
                        if (browser.ToLower() == "ie")
                        {
                            driver = new InternetExplorerDriver();
                            
                        }
                        else
                        {
                            if (browser.ToLower() == "safari")
                            {
                                driver = new SafariDriver();
                            }

                        }

                    }
                }

            }
        }// end of SelectDriverBrowser Method

        public string getproperty(string propValue)
        {
            string configpropertyFileName = "config.property";
            //string configpropertyFilePath = @"C:\DiplyProjects\qa-automation\qa-automation\Utilities\Resources\";
            string configpropertyFilePath = $@"{AppDomain.CurrentDomain.BaseDirectory}..\..\Utilities\Resources\{configpropertyFileName}";

            string[] lines = File.ReadAllLines(configpropertyFilePath);
            for (int i = 0; i < lines.Length; i++)
            {
                string probTitle = Regex.Split(lines[i], "=")[0].Trim();
                if (probTitle == propValue)
                    return Regex.Split(lines[i], "=")[1].Trim();
            }
            return ("Specified property is not Available!");
        }// end of getproperty Method

        public string DateForName()
        {
            DateTime localDate = DateTime.Now;
            // Year gets.
            int year = localDate.Year;
            // Month gets.
            int month = localDate.Month;
            // Day gets.
            int day = localDate.Day;
            // Hour gets.
            int hour = localDate.Hour;
            // Minute gets.
            int minute = localDate.Minute;
            // Second gets.
            int second = localDate.Second;
            // Millisecond.
            int millisecond = localDate.Millisecond;
            string ToName = $"{year.ToString()}{month.ToString()}{day.ToString()}-{hour.ToString()}{minute.ToString()}-{second.ToString()}{millisecond.ToString()}";
            return ToName;
        }// end of DateForName Method


        public void GreenMessage(String msg)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine(msg);
        }// end of GreenMessage Method

        public void RedMessage(String msg)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(msg);
        }// end of RedMessage Method

        // To create proper URL by getting data from Config file via UI
        public string CreateUrlToGo()
        {
            string URL_TOGO = "";
            string BROWSER_TOGO = "";
            string API_TOGO = "";

            URL_TOGO = getproperty("URL_TOGO");
            BROWSER_TOGO = getproperty("BROWSER_TOGO");
            API_TOGO = getproperty("API_TOGO");

             if (API_TOGO == "NULL")
            {
                return URL_TOGO; ;
            }
            else
            {
                return ( URL_TOGO + "?api-version=" + API_TOGO);
            }

        }// end of CreateUrlToGo Method



        // To create proper URL with recived parameters from command line
        public string UrlFactory(string envUrl, string envApi, string instance)
        {
            string URL_TOGO = "";
            string API_TOGO = "";
            string INSTANCE_TOGO = "";
            
            // Select The API
            if (envApi.ToLower() == "reskin")
            {
                API_TOGO = "3.reskin";
            }
            else
            {
                if (envApi.ToLower() == "hybrid")
                {
                    API_TOGO = "2.hybrid";
                }
                else
                {
                    if (envApi.ToLower() == "2v2")
                    {
                        API_TOGO = "2.v2";
                    }
                    else
                    {
                        API_TOGO = "";
                    }
                }
            }


            // Select the main URL
            if (envUrl.ToLower() == "qa")
            {
                URL_TOGO = "http://diplyqa.com/";
                return (URL_TOGO + "?api-version=" + API_TOGO);
            }
            else
            {
                if (envUrl.ToLower() == "staging")
                {
                    INSTANCE_TOGO = instance.ToLower();
                    URL_TOGO = "http://diplyweb" + INSTANCE_TOGO + ".azurewebsites.net/";
                    return (URL_TOGO + "?api-version=" + API_TOGO);
                }
                else
                {
                    URL_TOGO = "https://diply.com";
                    return URL_TOGO;
                }
            }

        }




        // Verify an Element Exist
        public bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }

}
