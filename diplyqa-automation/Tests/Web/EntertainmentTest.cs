﻿using diplyqa_automation.Pages.Web;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Threading;

namespace diplyqa_automation.Tests.Web
{
    [TestFixture]
    [Category("Regression")]
    class EntertainmentTest : Utilities.Base
    {

        public IWebDriver driverExt;
        [SetUp]
        public void OpenBrowser()
        {
            string envUrl = "";
            string envApi = "";
            string instance = "";
            string BROWSER_TOGO = "";

            envUrl = TestContext.Parameters["envUrl"];
            envApi = TestContext.Parameters["envApi"];
            instance = TestContext.Parameters["instance"];

            BROWSER_TOGO = getproperty("BROWSER_TOGO");

            SelectDriverBrowser(BROWSER_TOGO);
            driver.Manage().Window.Maximize();

            // To Let script works from Command line and VS UI
            // Via command line it might be controlled by parameters
            // Via VS Ui it might be controlled from config.property file
            if ((envUrl == null) && (envApi == null) && (instance == null))
            {
                driver.Navigate().GoToUrl(CreateUrlToGo());
            }
            else
            {
                driver.Navigate().GoToUrl(UrlFactory(envUrl, envApi, instance));
            }
        }


        [Test]
        [Category("_2x")]
        public void EntertainmentPageCard_2x()
        {
            test = extent.CreateTest("EntertainmentPageCard_2x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            var articlePage = new ArticlePage();
            var socialmediaPage = new SocialMediaPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);
            PageFactory.InitElements(driver, articlePage);
            PageFactory.InitElements(driver, socialmediaPage);
            Thread.Sleep(2000);

            headerPage.EntertainmentLink2x.Click();

            Thread.Sleep(3000);

            //Console.WriteLine(catPage.CatFunnyCardTitle3x.Text);
            string cardText, cardTextHerf;
            int ss = 1; int counter = 1;
            string[][] cardsInfo = new string[175][];
            string FBExtLink = "", TwtExtLink = "", TmlExtLink = "", GoglExtLink = "", pinlExtLink = "";

            // Read all cards' URL and Description
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(1, 1000)");
            for (int j = 1; j < 10; j++)
            {
                for (int i = ss; i < ss + 20; i++)
                {
                    if (IsElementPresent(By.XPath(catPage.EntertainmentCardTitle2x + "[" + i + "]")))
                    {

                        cardText = driver.FindElement(By.XPath(catPage.EntertainmentCardTitle2x + "[" + i + "]")).Text;
                        cardTextHerf = driver.FindElement(By.XPath(catPage.EntertainmentCardTitle2x + "[" + i + "]/div/div/a")).GetAttribute("href");
                        cardsInfo[i] = new string[2] { cardText, cardTextHerf };
                        counter = i;

                    }
                    else
                    {
                        break;
                    }
                }
                        ss = counter;
                ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(" + j * 1000 + ", " + j * 2000 + ")");

            }

            // Using Card info to verify Article page Elements
            for (int k = 1; k <= counter - 1; k++)
            {

                string cardText2 = cardsInfo[k][0];
                string cardTextHerf2 = cardsInfo[k][1];
               // string nextCardtext2 = cardsInfo[k + 1][0];

                driver.Navigate().GoToUrl(cardTextHerf2);
                Thread.Sleep(5000);

                if ((driver.FindElement(By.XPath(articlePage.Articeltitle_2xnew)).Displayed) == true)
                {

                    Assert.Multiple(() =>
                    {
                        try
                        {

                            // Verify URL and Article Description
                            Assert.That(driver.Url.Contains(cardTextHerf2));
                            Assert.That(true, cardText2, (driver.FindElement(By.XPath(articlePage.Articeltitle_2xnew))).Text);

                            // Facebook link verification
                            FBExtLink = driver.FindElement(By.XPath(articlePage.FaceBook_Button_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(FBExtLink);
                            Thread.Sleep(2000);
                            Assert.That(true, "Log in to your Facebook account to share.", driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginText)).Text);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginText)).Displayed);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginButton)).Enabled);
                            driverExt.Close();
                            GreenMessage("Facebook Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Twitter link verification
                            TwtExtLink = driver.FindElement(By.XPath(articlePage.Twitter_Button_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(TwtExtLink);
                            Thread.Sleep(3000);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Twitter_VerifyText)).Text.Contains(cardText2));
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Twitter_LoginButton)).Enabled);
                            driverExt.Close();
                            GreenMessage("Twitter Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Share button
                            driver.FindElement(By.XPath(articlePage.Share_Button_2x)).Click();

                            // Tumblr Link Verification
                            TmlExtLink = driver.FindElement(By.XPath(articlePage.Tumblr_button_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(TmlExtLink);
                            Thread.Sleep(3000);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Tumble_Post_button)).Enabled);
                            driverExt.Close();
                            GreenMessage("Tumblr Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Google Plus Link verfication
                            GoglExtLink = driver.FindElement(By.XPath(articlePage.GooglePlus_button_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(GoglExtLink);
                            Thread.Sleep(3000);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.GooglePlus_Login_text)).Displayed);
                            driverExt.Close();
                            GreenMessage("Google Plus Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Pinterest Link verfication
                            pinlExtLink = driver.FindElement(By.XPath(articlePage.Pinterest_button_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(pinlExtLink);
                            Thread.Sleep(3000);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Pinterest_Continue_Button)).Enabled);
                            driverExt.Close();
                            GreenMessage("Pinterest Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Verify Header Section
                            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(1, 1000)");
                            Assert.That(true, cardText2, (driver.FindElement(By.XPath(articlePage.ArticleTitleInHeader_2x))).Text);

                            // Facebook link in Header verification
                            FBExtLink = driver.FindElement(By.XPath(articlePage.FaceBook_ButtonInHeader_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(FBExtLink);
                            Thread.Sleep(2000);
                            Assert.That(true, "Log in to your Facebook account to share.", driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginText)).Text);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginText)).Displayed);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginButton)).Enabled);
                            driverExt.Close();
                            GreenMessage("Facebook Icon in header for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Twitter link verification
                            TwtExtLink = driver.FindElement(By.XPath(articlePage.Twitter_ButtonInHeader_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(TwtExtLink);
                            Thread.Sleep(3000);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Twitter_VerifyText)).Text.Contains(cardText2));
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Twitter_LoginButton)).Enabled);
                            driverExt.Close();
                            GreenMessage("Twitter Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Verify Next button in header is enabled 
                            Assert.That(driver.FindElement(By.XPath(articlePage.nextButtonInHeader_2x)).Displayed);
                            Assert.That(driver.FindElement(By.XPath(articlePage.nextButtonInHeader_2x)).Enabled);

                            Console.WriteLine(k + " - " + cardsInfo[k][0], cardsInfo[k][1]);

                        }
                        catch (Exception e)
                        {
                            RedMessage("Funny Article Page with this subject {" + cardText2 + "} is not loading properly. It was timeout.");

                            throw (e);
                        }

                    });
                    GreenMessage("Funny Article Page with this subject {" + cardText2 + "} displays properly!");

                }
                else
                {
                    RedMessage("Funny Article Page with this subject {" + cardText2 + "} is not loading properly, or a link in that page is not properly set!");
                }
                driver.Navigate().Back();
                Thread.Sleep(3000);

            }

        }


        [Test]
        [Category("_3x")]
        public void EntertainmentPageCard_3x()
        {
            test = extent.CreateTest("EntertainmentPageCard_3x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            var articlePage = new ArticlePage();
            var socialmediaPage = new SocialMediaPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);
            PageFactory.InitElements(driver, articlePage);
            PageFactory.InitElements(driver, socialmediaPage);
            Thread.Sleep(2000);

            headerPage.EntertainmentLink3x.Click();

            Thread.Sleep(3000);

            //Console.WriteLine(catPage.CatFunnyCardTitle3x.Text);
            string cardText, cardTextHerf;
            int ss = 1; int counter = 1;
            string[][] cardsInfo = new string[175][];
            string FBExtLink = "", TwtExtLink = "", TmlExtLink = "", GoglExtLink = "", pinlExtLink = "";

            // Read all cards' URL and Description
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(1, 1000)");
            for (int j = 1; j < 10; j++)
            {
                for (int i = ss; i < ss + 20; i++)
                {
                    if (IsElementPresent(By.XPath(catPage.EntertainmentCardTitle3x + "[" + i + "]")))
                    {

                        cardText = driver.FindElement(By.XPath(catPage.EntertainmentCardTitle3x + "[" + i + "]/div/a[2]")).Text;
                        cardTextHerf = driver.FindElement(By.XPath(catPage.EntertainmentCardTitle3x + "[" + i + "]/div/a[2]")).GetAttribute("href");
                        cardsInfo[i] = new string[2] { cardText, cardTextHerf };
                        counter = i;

                    }
                    else
                    {
                        break;
                    }
                }
                ss = counter;
                ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(" + j * 1000 + ", " + j * 2000 + ")");

            }

            // Using Card info to verify Article page Elements
            for (int k = 1; k <= counter; k++)
            {

                string cardText2 = cardsInfo[k][0];
                string cardTextHerf2 = cardsInfo[k][1];
                // string nextCardtext2 = cardsInfo[k + 1][0];

                driver.Navigate().GoToUrl(cardTextHerf2);
                Thread.Sleep(5000);

                if ((driver.FindElement(By.XPath(articlePage.Articeltitle_2xnew)).Displayed) == true)
                {

                    Assert.Multiple(() =>
                    {
                        try
                        {

                            // Verify URL and Article Description
                            Assert.That(driver.Url.Contains(cardTextHerf2));
                            Assert.That(true, cardText2, (driver.FindElement(By.XPath(articlePage.Articeltitle_2xnew))).Text);

                            // Facebook link verification
                            FBExtLink = driver.FindElement(By.XPath(articlePage.FaceBook_Button_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(FBExtLink);
                            Thread.Sleep(2000);
                            Assert.That(true, "Log in to your Facebook account to share.", driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginText)).Text);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginText)).Displayed);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginButton)).Enabled);
                            driverExt.Close();
                            GreenMessage("Facebook Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Twitter link verification
                            TwtExtLink = driver.FindElement(By.XPath(articlePage.Twitter_Button_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(TwtExtLink);
                            Thread.Sleep(3000);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Twitter_VerifyText)).Text.Contains(cardText2));
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Twitter_LoginButton)).Enabled);
                            driverExt.Close();
                            GreenMessage("Twitter Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Share button
                            driver.FindElement(By.XPath(articlePage.Share_Button_2x)).Click();

                            // Tumblr Link Verification
                            TmlExtLink = driver.FindElement(By.XPath(articlePage.Tumblr_button_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(TmlExtLink);
                            Thread.Sleep(3000);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Tumble_Post_button)).Enabled);
                            driverExt.Close();
                            GreenMessage("Tumblr Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Google Plus Link verfication
                            GoglExtLink = driver.FindElement(By.XPath(articlePage.GooglePlus_button_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(GoglExtLink);
                            Thread.Sleep(3000);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.GooglePlus_Login_text)).Displayed);
                            driverExt.Close();
                            GreenMessage("Google Plus Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Pinterest Link verfication
                            pinlExtLink = driver.FindElement(By.XPath(articlePage.Pinterest_button_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(pinlExtLink);
                            Thread.Sleep(3000);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Pinterest_Continue_Button)).Enabled);
                            driverExt.Close();
                            GreenMessage("Pinterest Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Verify Header Section
                            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(1, 1000)");
                            Assert.That(true, cardText2, (driver.FindElement(By.XPath(articlePage.ArticleTitleInHeader_2x))).Text);

                            // Facebook link in Header verification
                            FBExtLink = driver.FindElement(By.XPath(articlePage.FaceBook_ButtonInHeader_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(FBExtLink);
                            Thread.Sleep(2000);
                            Assert.That(true, "Log in to your Facebook account to share.", driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginText)).Text);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginText)).Displayed);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.FaceBook_LoginButton)).Enabled);
                            driverExt.Close();
                            GreenMessage("Facebook Icon in header for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Twitter link verification
                            TwtExtLink = driver.FindElement(By.XPath(articlePage.Twitter_ButtonInHeader_2x)).GetAttribute("href");
                            driverExt = new ChromeDriver();
                            driverExt.Navigate().GoToUrl(TwtExtLink);
                            Thread.Sleep(3000);
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Twitter_VerifyText)).Text.Contains(cardText2));
                            Assert.That(driverExt.FindElement(By.XPath(socialmediaPage.Twitter_LoginButton)).Enabled);
                            driverExt.Close();
                            GreenMessage("Twitter Icon for {" + cardText2 + "} works properly!");
                            Thread.Sleep(1000);

                            // Verify Next button in header is enabled 
                            Assert.That(driver.FindElement(By.XPath(articlePage.nextButtonInHeader_2x)).Displayed);
                            Assert.That(driver.FindElement(By.XPath(articlePage.nextButtonInHeader_2x)).Enabled);

                            Console.WriteLine(k + " - " + cardsInfo[k][0], cardsInfo[k][1]);

                        }
                        catch (Exception e)
                        {
                            RedMessage("Funny Article Page with this subject {" + cardText2 + "} is not loading properly. It was timeout.");

                            throw (e);
                        }

                    });
                    GreenMessage("Funny Article Page with this subject {" + cardText2 + "} displays properly!");

                }
                else
                {
                    RedMessage("Funny Article Page with this subject {" + cardText2 + "} is not loading properly, or a link in that page is not properly set!");
                }
                driver.Navigate().Back();
                Thread.Sleep(3000);

            }

        }


        [TearDown]
        public void CleanUp()
        {
            driver.Close();
        }

    }
}

