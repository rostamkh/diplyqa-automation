﻿using diplyqa_automation.Pages.Web;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace diplyqa_automation.Tests.Web
{
    [TestFixture]
    [Category("Regression")]
    class HeaderTest : Utilities.Base
    {
 /*       [SetUp]
        public void OpenBrowser()
        {
            string BROWSER_TOGO = "";
            BROWSER_TOGO = getproperty("BROWSER_TOGO");

            SelectDriverBrowser(BROWSER_TOGO);
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(CreateUrlToGo());
        }
*/
        [SetUp]
        public void OpenBrowser()
        {
            string envUrl = "";
            string envApi = "";
            string instance = "";
            string BROWSER_TOGO = "";

            envUrl = TestContext.Parameters["envUrl"];
            envApi = TestContext.Parameters["envApi"];
            instance = TestContext.Parameters["instance"];

            BROWSER_TOGO = getproperty("BROWSER_TOGO");

            SelectDriverBrowser(BROWSER_TOGO);
//            driver.Manage().Window.Maximize();

            // To Let script works from Command line and VS UI
            // Via command line it might be controlled by parameters
            // Via VS Ui it might be controlled from config.property file
            if ((envUrl ==null) && (envApi == null) && (instance == null))
            {
                driver.Navigate().GoToUrl(CreateUrlToGo());
            }
            else
            {
                driver.Navigate().GoToUrl(UrlFactory(envUrl, envApi, instance));
            }
        }


        [Test]
        [Category("_2x")]
        public void Home_Entertaimnet_Link_2x()
        {
            test = extent.CreateTest("Home_Entertaimnet_Link_2x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);

            //           if (!(driver.Url.Contains("entertainment-life")))
            //           {
            //Thread.Sleep(5000);
            headerPage.EntertainmentLink2x.Click();
            Thread.Sleep(5000);
            //           }

            Assert.That(driver.Url.Contains("entertainment-life"));
            Assert.AreEqual("ENTERTAINMENT", catPage.CatEntertainmentTitle2x.Text, "Category Title is not correct!");
        }

        [Test]
        [Category("_2x")]
        public void Home_Funny_Link_2x()
        {
            test = extent.CreateTest("Home_Funny_Link_2x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);

            //           if (!(driver.Url.Contains("funny-stuff")))
            //           {
            headerPage.FunnyLink2x.Click();
            Thread.Sleep(5000);
            //           }

            Assert.That(driver.Url.Contains("funny-stuff"));
            Assert.AreEqual("FUNNY", catPage.CatFunnyTitle2x.Text, "Category Title is not correct!");
        }

        [Test]
        [Category("_2x")]
        public void Home_Life_Link_2x()
        {
            test = extent.CreateTest("Home_Life_Link_2x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);

            //           if (!(driver.Url.Contains("life")))
            //           {
            headerPage.LifeLink2x.Click();
            Thread.Sleep(5000);
            //           }

            Assert.That(driver.Url.Contains("life"));
            Assert.AreEqual("LIFE", catPage.CatLifeTitle2x.Text, "Category Title is not correct!");

        }

        [Test]
        [Category("_2x")]
        public void Home_Inspirational_Link_2x()
        {
            test = extent.CreateTest("Home_Inspirational_Link_2x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);

            //           if (!(driver.Url.Contains("inspirational")))
            //           {
            headerPage.InspirationLink2x.Click();
            Thread.Sleep(5000);
            //           }

            Assert.That(driver.Url.Contains("inspirational"));
            Assert.AreEqual("INSPIRATIONAL", catPage.CatInspirationalTitle2x.Text, "Category Title is not correct!");
        }

        [Test]
        [Category("_2x")]
        public void Home_Videos_Link_2x()
        {
            test = extent.CreateTest("Home_Videos_Link_2x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);

            //           if (!(driver.Url.Contains("videos")))
            //           {
            headerPage.VideosLink2x.Click();
            Thread.Sleep(5000);
            //           }


            Assert.That(driver.Url.Contains("videos"));
            Assert.AreEqual("VIDEOS", catPage.CatVideosTitle2x.Text, "Category Title is not correct!");
        }


        [Test]
        [Category("_2x")]
        public void Home_Link_2x()
        {
            test = extent.CreateTest("Home_Link_2x");

            var headerPage = new HeaderPage();
            var homePage = new HomePage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, homePage);

            if (driver.Url.Contains("category"))
            {
                headerPage.HomeLink2x.Click();
                Thread.Sleep(5000);
            }

            Assert.That(driver.PageSource.Contains("feed-feature video"));
            Assert.That(driver.PageSource.Contains("feed-feature mini"));
            Assert.That(driver.PageSource.Contains("feed-feature"));


        }



        /*
         * Diply 3.x hybrid ->
         */



        [Test]
        [Category("_3x")]        
        public void Home_Entertaimnet_Link_3x()
        {
            test = extent.CreateTest("Home_Entertaimnet_Link_3x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);

            //           if (!(driver.Url.Contains("entertainment-life")))
            //           {
            headerPage.EntertainmentLink3x.Click();
            Thread.Sleep(5000);
            //           }

            Assert.That(driver.Url.Contains("entertainment-life"));
            Assert.AreEqual("ENTERTAINMENT", catPage.CatEntertainmentTitle3x.Text, "Category Title is not correct!");
        }


        [Test]
        [Category("_3x")]
        public void Home_Funny_Link_3x()
        {
            test = extent.CreateTest("Home_Funny_Link_3x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);

            //           if (!(driver.Url.Contains("funny-stuff")))
            //           {
            headerPage.FunnyLink3x.Click();
            Thread.Sleep(5000);
            //           }

            Assert.That(driver.Url.Contains("funny-stuff"));
            Assert.AreEqual("FUNNY", catPage.CatFunnyTitle3x.Text, "Category Title is not correct!");
        }

        [Test]
        [Category("_3x")]
        public void Home_Life_Link_3x()
        {
            test = extent.CreateTest("Home_Life_Link_3x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);

            //           if (!(driver.Url.Contains("life")))
            //           {
            headerPage.LifeLink3x.Click();
            Thread.Sleep(5000);
            //           }

            Assert.That(driver.Url.Contains("life"));
            Assert.AreEqual("LIFE", catPage.CatLifeTitle3x.Text, "Category Title is not correct!");

        }

        [Test]
        [Category("_3x")]
        public void Home_Inspirational_Link_3x()
        {
            test = extent.CreateTest("Home_Inspirational_Link_3x");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);

            //           if (!(driver.Url.Contains("inspirational")))
            //           {
            headerPage.InspirationLink3x.Click();
            Thread.Sleep(5000);
            //           }

            Assert.That(driver.Url.Contains("inspirational"));
            Assert.AreEqual("INSPIRATIONAL", catPage.CatInspirationalTitle3x.Text, "Category Title is not correct!");
        }

        [Test]
        [Category("_3x")]
        public void Home_Videos_Link_3x()
        {
            test = extent.CreateTest("Home_Videos_Link_3");

            var headerPage = new HeaderPage();
            var catPage = new CategoryPage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, catPage);

            //           if (!(driver.Url.Contains("videos")))
            //           {
            headerPage.VideosLink3x.Click();
            Thread.Sleep(5000);
            //           }


            Assert.That(driver.Url.Contains("videos"));
            Assert.AreEqual("VIDEOS", catPage.CatVideosTitle3x.Text, "Category Title is not correct!");
        }


        [Test]
        [Category("_3x")]
        public void Home_Link_3x()
        {
            test = extent.CreateTest("Home_Link_3x");

            var headerPage = new HeaderPage();
            var homePage = new HomePage();
            PageFactory.InitElements(driver, headerPage);
            PageFactory.InitElements(driver, homePage);

            if (driver.Url.Contains("category"))
            {
                headerPage.HomeLink3x.Click();
                Thread.Sleep(5000);
            }

            Assert.That(driver.PageSource.Contains("feed-video-container"));
            Assert.That(driver.PageSource.Contains("feed-card-container"));
            Assert.That(driver.PageSource.Contains("feed-list-container"));
        }

        [Test]
        [Category("_3x")]
        public void SocialMedia_Icons_3x()
        {
            test = extent.CreateTest("SocialMedia_Icons_3x");

            var headerPage = new HeaderPage();
            PageFactory.InitElements(driver, headerPage);

            Thread.Sleep(10000);

            headerPage.DiplyFaceBook3x_Top.Click();
 //           Assert.That(headerPage.DiplyFaceBook3x_Top.Displayed);
 //           Assert.That(headerPage.DiplyFaceBook3x_Top.Enabled);

            bool visible = IsElementVisible(headerPage.DiplyFaceBook3x_Top);
            Assert.That(visible);   
            if (visible)
            {
                GreenMessage("FaceBook Icon is displaying and it is Enabled");
            }
            else
            {
                RedMessage("FaceBook Icon is NOt displaying NOR it is Enabled");
            }


        }

        /// <summary>
        /// ////////////////////////
        /// </summary>

        public bool IsElementVisible(IWebElement element)
        {
            return element.Displayed && element.Enabled;
        }


        /// <summary>
        /// ////////////////////////
        /// </summary>

        [TearDown]
        public void CleanUp()
        {
            driver.Close();
        }

    }
}
