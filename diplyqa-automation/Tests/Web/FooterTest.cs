﻿using diplyqa_automation.Pages.Web;
using NUnit.Framework;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Threading;

namespace diplyqa_automation.Tests.Web
{
    [TestFixture]
    class FooterTest : Utilities.Base
    {


        [SetUp]
        public void OpenBrowser()
        {
            string envUrl = "";
            string envApi = "";
            string instance = "";
            string BROWSER_TOGO = "";

            envUrl = TestContext.Parameters["envUrl"];
            envApi = TestContext.Parameters["envApi"];
            instance = TestContext.Parameters["instance"];

            BROWSER_TOGO = getproperty("BROWSER_TOGO");

            SelectDriverBrowser(BROWSER_TOGO);
            //            driver.Manage().Window.Maximize();

            // To Let script works from Command line and VS UI
            // Via command line it might be controlled by parameters
            // Via VS Ui it might be controlled from config.property file
            if ((envUrl == null) && (envApi == null) && (instance == null))
            {
                driver.Navigate().GoToUrl(CreateUrlToGo());
            }
            else
            {
                driver.Navigate().GoToUrl(UrlFactory(envUrl, envApi, instance));
            }
        }


        [TearDown]
        public void CleanUp()
        {
            driver.Close();
        }


        [Test]
        public void FooterLink_AboutUs_3x()
        {
            test = extent.CreateTest("FooterLink_AboutUs_3x");

            var footerPage = new FooterPage();
            var aboutUsPage = new AboutUsPage();
            PageFactory.InitElements(driver, footerPage);
            PageFactory.InitElements(driver, aboutUsPage);

            footerPage.DiplyAboutUs3x_Bottom.Click();
            Thread.Sleep(8000);
            Console.WriteLine("URL_GO =Hey Here   ->   " + driver.Url);
            Assert.That(driver.Url.Contains("about"));
            Assert.AreEqual("ABOUT US", aboutUsPage.DiplyAboutUs3x_Title.Text, "About Us Title is not correct!");

        }


        [Test]
        [Category("_3x")]
        public void FooterLink_Advertise_3x()
        {
            test = extent.CreateTest("FooterLink_Advertise_3x");

            var footerPage = new FooterPage();
            var advertisePage = new AdvertisePage();
            PageFactory.InitElements(driver, footerPage);
            PageFactory.InitElements(driver, advertisePage);

            footerPage.DiplyAdvertise3x_Bottom.Click();
            Thread.Sleep(8000);

            Assert.That(driver.Url.Contains("advertise"));
            Assert.AreEqual("ABOUT US", advertisePage.DiplyAdvertise3x_Title.Text, "Advertise Title is not correct!");

        }



        [Test]
        [Category("_3x")]
        public void FooterLink_Careers_3x()
        {
            test = extent.CreateTest("FooterLink_Careers_3x");

            var footerPage = new FooterPage();
            var carriersPage = new CareersPage();
            PageFactory.InitElements(driver, footerPage);
            PageFactory.InitElements(driver, carriersPage);

            footerPage.DiplyCareers3x_Bottom.Click();
            Thread.Sleep(8000);

            Assert.That(driver.Url.Contains("careers"));
            Assert.AreEqual("OUR STORY", carriersPage.DiplyCareers3x_Title.Text, "Careers Title is not correct!");

        }



        [Test]
        [Category("_3x")]
        public void FooterLink_TermsOfUse_3x()
        {
            test = extent.CreateTest("FooterLink_TermsOfUse_3x");

            var footerPage = new FooterPage();
            var termOfUsePage = new TermsOfUsePage();
            PageFactory.InitElements(driver, footerPage);
            PageFactory.InitElements(driver, termOfUsePage);

            footerPage.DiplyTermsOfUse3x_Bottom.Click();
            Thread.Sleep(8000);

            Assert.That(driver.Url.Contains("terms"));
            Assert.AreEqual("TERMS OF USE", termOfUsePage.DiplyTermsOfUse3x_Title.Text, "Terms Of Use Title is not correct!");

        }



        [Test]
        [Category("_3x")]
        public void FooterLink_TermsOfServices_3x()
        {
            test = extent.CreateTest("FooterLink_TermsOfServices_3x");

            var footerPage = new FooterPage();
            var termsOfServicesPage = new TermsOfServicesPage();
            PageFactory.InitElements(driver, footerPage);
            PageFactory.InitElements(driver, termsOfServicesPage);

            footerPage.DiplyTermsOfServices3x_Bottom.Click();
            Thread.Sleep(8000);

            Assert.That(driver.Url.Contains("socialcommerce"));
            Assert.AreEqual("TERMS OF SERVICES", termsOfServicesPage.DiplyTermsOfServices3x_Title.Text, "Terms Of Services Title is not correct!");

        }



        [Test]
        [Category("_3x")]
        public void FooterLink_PrivacyPolicy_3x()
        {
            test = extent.CreateTest("FooterLink_PrivacyPolicy_3x");

            var footerPage = new FooterPage();
            var privacyPolicyPage = new PrivacyPolicyPage();
            PageFactory.InitElements(driver, footerPage);
            PageFactory.InitElements(driver, privacyPolicyPage);

            footerPage.DiplyPrivacyPolicy3x_Bottom.Click();
            Thread.Sleep(8000);

            Assert.That(driver.Url.Contains("privacy"));
            Assert.AreEqual("PRIVACY POLICY", privacyPolicyPage.DiplyPrivacyPolicy3x_Title.Text, "privacy Policy Page Title is not correct!");

        }

        [Test]
        [Category("_3x")]
        public void FooterLink_SubmitYourVideo_3x()
        {
            test = extent.CreateTest("FooterLink_SubmitYourVideo_3x");

            var footerPage = new FooterPage();
            var submitYourVideoPage = new SubmitYourVideoPage();
            PageFactory.InitElements(driver, footerPage);
            PageFactory.InitElements(driver, submitYourVideoPage);

            footerPage.DiplySubmitVideo3x_Bottom.Click();
            Thread.Sleep(8000);

            //           Console.WriteLine("Here is the URL!  -> " + driver.Url);
            Assert.That(driver.Url.Contains("exec"));
            if (driver.Url.Contains("exec"))
            {
                GreenMessage("Here is the URL from green!  -> " + driver.Url);
            }
            else
            {
                RedMessage("URL is Not correct!  -> " + driver.Url);
            }
             Assert.That(driver.PageSource.Contains("Submit Your Video"));
        }
    }
}
