﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace diplyqa_automation.Pages.Web
{
    class CategoryPage
    {


        /*
       * Diply 2.x -> /?api-version=2.reskin
       */
        public string EntertainmentCardTitle2x = ".//*[@class='feed-item article ']";
        public string FunnyCardTitle2x = ".//*[@class='feed-item article ']";
        public string LifeCardTitle2x = ".//*[@class='feed-item article ']";
        public string InspirationalCardTitle2x = ".//*[@class='feed-item article ']";
        public string VideoCardTitle2x = ".//*[@class='feed-item article ']";



        // Entertaiment Ver 2x
        [FindsBy(How = How.XPath, Using = ".//*[@class='cat-auth'][contains(h2, 'Entertainment')]")]
        [CacheLookup]
        public IWebElement CatEntertainmentTitle2x { get; set; }

        // Funny Ver 2x
        [FindsBy(How = How.XPath, Using = ".//*[@class='cat-auth'][contains(h2, 'Funny')]")]
        [CacheLookup]
        public IWebElement CatFunnyTitle2x { get; set; }

        // Life Ver 2x
        [FindsBy(How = How.XPath, Using = ".//*[@class='cat-auth'][contains(h2, 'Life')]")]
        [CacheLookup]
        public IWebElement CatLifeTitle2x { get; set; }

        // Inspirational Ver 2x
        [FindsBy(How = How.XPath, Using = ".//*[@class='cat-auth'][contains(h2, 'Inspirational')]")]
        [CacheLookup]
        public IWebElement CatInspirationalTitle2x { get; set; }

        // Videos Ver 2x
        [FindsBy(How = How.XPath, Using = ".//*[@class='cat-auth'][contains(h2, 'Videos')]")]
        [CacheLookup]
        public IWebElement CatVideosTitle2x { get; set; }



        /*
         * Diply 3.x hybrid ->
         */

        // Entertaiment Ver 3x
        public string EntertainmentCardTitle3x = ".//*[@class='card']";
        public string FunnyCardTitle3x = ".//*[@class='card']";
        public string LifeCardTitle3x = ".//*[@class='card']";
        public string InspirationalCardTitle3x = ".//*[@class='card']";
        public string VideoCardTitle3x = ".//*[@class='card']";




        [FindsBy(How = How.XPath, Using = ".//*[@class='category-wrapper entertainment-life'][contains(h2, 'Entertainment')]")]
        [CacheLookup]
        public IWebElement CatEntertainmentTitle3x { get; set; }

        // Funny Ver 3x
        [FindsBy(How = How.XPath, Using = ".//*[@class='category-wrapper funny-stuff'][contains(h2, 'Funny')]")]
        [CacheLookup]
        public IWebElement CatFunnyTitle3x { get; set; }

        
        
        [FindsBy(How = How.CssSelector, Using = "div.content-text")]
        [CacheLookup]
        public IWebElement CatFunnyCardTitle3x { get; set; }
        

        // Life Ver 3x
        [FindsBy(How = How.XPath, Using = ".//*[@class='category-wrapper life'][contains(h2, 'Life')]")]
        [CacheLookup]
        public IWebElement CatLifeTitle3x { get; set; }

        // Inspirational Ver 3x
        [FindsBy(How = How.XPath, Using = ".//*[@class='category-wrapper inspirational'][contains(h2, 'Inspirational')]")]
        [CacheLookup]
        public IWebElement CatInspirationalTitle3x { get; set; }

        // Videos Ver 3x
        [FindsBy(How = How.XPath, Using = ".//*[@class='category-wrapper videos'][contains(h2, 'Videos')]")]
        [CacheLookup]
        public IWebElement CatVideosTitle3x { get; set; }

    }
}
