﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace diplyqa_automation.Pages.Web
{
    class TermsOfUsePage
    {


        /*
         * Diply 2.x -> /?api-version=2.reskin
         */






        /*
         * Diply 3.x hybrid ->
         */

        [FindsBy(How = How.XPath, Using = ".//section[@class='terms']/h1")]
        [CacheLookup]
        public IWebElement DiplyTermsOfUse3x_Title { get; set; }
    }
}
