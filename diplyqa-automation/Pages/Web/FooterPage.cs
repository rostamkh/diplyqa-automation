﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace diplyqa_automation.Pages.Web
{
    public class FooterPage
    {

        /*
         * Diply 2.x -> /?api-version=2.reskin
         */






        /*
         * Diply 3.x hybrid ->
         */

        // Social Media Icons
        // Diply Facebook Bottom Ver 3.x
        [FindsBy(How = How.ClassName, Using = ".//ul[@class='engagement external-properties']/li/a[@class='soc-btn sharing-fb fa-stack fa-lg']")]
        //.//*[@class='menu-bar-right social-icons']/ul/li/a[@class='soc-btn sharing-fb fa-stack fa-lg']
        [CacheLookup]
        public IWebElement DiplyFaceBook3x_Bottom { get; set; }


        // Menu bar
        // About US
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'About Us')]/a")]
        [CacheLookup]
        public IWebElement DiplyAboutUs3x_Bottom { get; set; }


        // Advertise
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Advertise')]/a")]
        [CacheLookup]
        public IWebElement DiplyAdvertise3x_Bottom { get; set; }


        // Careers
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Careers')]/a")]
        [CacheLookup]
        public IWebElement DiplyCareers3x_Bottom { get; set; }


        // Terms of Use
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Terms of Use')]/a")]
        [CacheLookup]
        public IWebElement DiplyTermsOfUse3x_Bottom { get; set; }


        // Terms of Services
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Terms of Services')]/a")]
        [CacheLookup]
        public IWebElement DiplyTermsOfServices3x_Bottom { get; set; }


        // Privacy Policy
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Privacy Policy')]/a")]
        [CacheLookup]
        public IWebElement DiplyPrivacyPolicy3x_Bottom { get; set; }


        // SUBMIT VIDEO
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'SUBMIT VIDEO')]/a")]
        [CacheLookup]
        public IWebElement DiplySubmitVideo3x_Bottom { get; set; }
    }
}
