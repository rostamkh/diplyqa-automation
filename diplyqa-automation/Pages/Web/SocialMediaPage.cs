﻿namespace diplyqa_automation.Pages.Web
{
    class SocialMediaPage
    {


        /*
       * Diply 2.x -> /?api-version=2.reskin
       */

        // FaceBook
        public string FaceBook_LoginText = ".//*[@id='content']";
        public string FaceBook_LoginButton = ".//*[@id='loginbutton']";


        //Twitter
        public string Twitter_ShareText = "action-information";
        public string Twitter_VerifyText = ".//*[@id='status']";
        public string Twitter_LoginButton = ".//*[@class='button selected submit']";

        // Other Social Media
        public string Pinterest_Continue_Button = ".//*[@class='red SignupButton active']";
        public string GooglePlus_Login_text = ".//*[@class='FgbZLd']";
        public string Tumble_Post_button = ".//*[@class='tx-button blue']";

    }
}
