﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace diplyqa_automation.Pages.Web
{
    public class SubmitYourVideoPage
    {


        /*
         * Diply 2.x -> /?api-version=2.reskin
         */






        /*
         * Diply 3.x hybrid ->
         */

        [FindsBy(How = How.XPath, Using = ".//*[@class='row']/div[contains(h4, 'Submit')]/h4")]
        [CacheLookup]
        public IWebElement DiplySubmitYourVideo3x_Title { get; set; }


 
    }
}

