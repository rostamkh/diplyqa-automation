﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace diplyqa_automation.Pages.Web
{
    class HomePage
    {

        // Feed Feature Video Ver 2x
        [FindsBy(How = How.XPath, Using = ".//*[@class='feed-feature video']")]
        [CacheLookup]
        public IWebElement HomeVideofeature2x { get; set; }

        // Feed Feature Mini Ver 2x
        [FindsBy(How = How.XPath, Using = ".//*[@class='feed-feature mini']")]
        [CacheLookup]
        public IWebElement HomeMinifeature2x { get; set; }

        // Feed Feature Ver 2x
        [FindsBy(How = How.XPath, Using = ".//*[@class='feed-feature']")]
        [CacheLookup]
        public IWebElement Homefeature2x { get; set; }

    }
}
