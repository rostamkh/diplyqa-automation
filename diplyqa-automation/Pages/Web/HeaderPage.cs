﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace diplyqa_automation.Pages.Web
{
    public class HeaderPage
    {



        /*
         * Diply 2.x -> /?api-version=2.reskin
         */

        // Home Link Ver 2x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar'][1]/li[contains(a, 'Home')]/a")]
        [CacheLookup]
        public IWebElement HomeLink2x { get; set; }

        // ENTERTAINMENT Link Ver 2x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar'][1]/li[contains(a, 'Entertainment')]/a")]
        [CacheLookup]
        public IWebElement EntertainmentLink2x { get; set; }

        // Funny Link Ver 2x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar'][1]/li[contains(a, 'Funny')]/a")]
        //      [CacheLookup]
        public IWebElement FunnyLink2x { get; set; }

        // Life Link Ver 2x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar'][2]/li[contains(a, 'Life')]/a")]
        [CacheLookup]
        public IWebElement LifeLink2x { get; set; }

        // Inspiration Link Ver 2x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar'][2]/li[contains(a, 'Inspiration')]/a")]
        [CacheLookup]
        public IWebElement InspirationLink2x { get; set; }

        // Videos Link Ver 2x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar'][2]/li[contains(a, 'Videos')]/a")]
        [CacheLookup]
        public IWebElement VideosLink2x { get; set; }

        // Diply logo Ver 2x
        [FindsBy(How = How.ClassName, Using = "brand-logo")]
        [CacheLookup]
        public IWebElement DiplyLogo2x { get; set; }

        // Diply Hamberger Ver 2x
        [FindsBy(How = How.Id, Using = "feature-hamburger")]
        //  [CacheLookupA]
        public IWebElement DiplyHamberger2x { get; set; }




        /*
         * Diply 3.x hybrid ->
         */

        // Home Link Ver 3.x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Home')]")]
        [CacheLookup]
        public IWebElement HomeLink3x { get; set; }

        // ENTERTAINMENT Link Ver 3.x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Entertainment')]")]
        [CacheLookup]
        public IWebElement EntertainmentLink3x { get; set; }

        // Funny Link Ver 3.x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Funny')]")]
        [CacheLookup]
        public IWebElement FunnyLink3x { get; set; }

        // Life Link Ver 3.x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Life')]")]
        [CacheLookup]
        public IWebElement LifeLink3x { get; set; }

        // Inspiration Link Ver 3.x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Inspiration')]")]
        [CacheLookup]
        public IWebElement InspirationLink3x { get; set; }

        // Videos Link Ver 3.x
        [FindsBy(How = How.XPath, Using = ".//ul[@class='menu-bar']/li[contains(a, 'Videos')]")]
        [CacheLookup]
        public IWebElement VideosLink3x { get; set; }

        // Diply logo Ver 3.x
        [FindsBy(How = How.ClassName, Using = "branding brand-logo")]
        [CacheLookup]
        public IWebElement DiplyLogo3x { get; set; }

        // Hamberger
        // No Hamberger for V3.0

        // Social Media Icons
        // Diply Facebook Top Ver 3.x
        [FindsBy(How = How.ClassName, Using = ".//*[@class='menu-bar-right social-icons']/ul/li/a[@class='soc-btn sharing-fb fa-stack fa-lg']")]
        //.//*[@class='menu-bar-right social-icons']/ul/li/a[@class='soc-btn sharing-fb fa-stack fa-lg']
        [CacheLookup]
        public IWebElement DiplyFaceBook3x_Top { get; set; }

        // Diply Instagram Top Ver 3.x
        [FindsBy(How = How.ClassName, Using = ".//*[@class='menu-bar-right social-icons']/ul/li/a[@class='soc-btn sharing-instagram fa-stack fa-lg']")]
        [CacheLookup]
        public IWebElement DiplyInstagram3x_Top { get; set; }

        // Diply Pinterest Top Ver 3.x
        [FindsBy(How = How.ClassName, Using = ".//*[@class='menu-bar-right social-icons']/ul/li/a[@class='soc-btn sharing-pin fa-stack fa-lg']")]
        [CacheLookup]
        public IWebElement DiplyPinterest3x_Top { get; set; }

        // Diply Twitter Top Ver 3.x
        [FindsBy(How = How.ClassName, Using = ".//*[@class='menu-bar-right social-icons']/ul/li/a[@class='soc-btn sharing-twitter fa-stack fa-lg']")]
        [CacheLookup]
        public IWebElement DiplyTwitter3x_Top { get; set; }

    }
}
