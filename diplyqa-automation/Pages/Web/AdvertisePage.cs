﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace diplyqa_automation.Pages.Web
{
    class AdvertisePage
    {


        /*
         * Diply 2.x -> /?api-version=2.reskin
         */






        /*
         * Diply 3.x hybrid ->
         */

        [FindsBy(How = How.XPath, Using = ".//*[@class='heroTitle']")]
        [CacheLookup]
        public IWebElement DiplyAdvertise3x_Title { get; set; }
    }
}
