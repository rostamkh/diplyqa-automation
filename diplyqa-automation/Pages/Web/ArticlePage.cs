﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace diplyqa_automation.Pages.Web
{
    class ArticlePage
    {



        /*
       * Diply 2.x -> /?api-version=2.reskin
       */



        // Entertaiment Ver 2x
        [FindsBy(How = How.XPath, Using = ".//*[@class='title-wrap']/h1")]
        [CacheLookup]
        public IWebElement ArticleTitle_2x { get; set; }

        public string Articeltitle_2xnew = ".//*[@class='title-wrap']/h1";        
        public string FaceBook_Button_2x = ".//*[@class='soc-btn fb-share-btn']";
        public string Twitter_Button_2x = ".//*[@class='social-row main-row']/a[@class='soc-btn sharing-twt']";
        public string Share_Button_2x = ".//*[@class='soc-btn sharing-toggle']";
        public string Pinterest_button_2x = ".//*[@class='soc-btn sharing-pin']";
        public string GooglePlus_button_2x = ".//*[@class='soc-btn sharing-gplus']";
        public string Tumblr_button_2x = ".//*[@class='soc-btn sharing-tumblr']";

        public string ArticleTitleInHeader_2x = ".//*[@id='header']/h5";
        public string FaceBook_ButtonInHeader_2x = ".//*[@class='fb-share-btn head-ask-btn show-info']";
        public string Twitter_ButtonInHeader_2x = ".//*[@class='sharing-twt']";
        public string nextButtonInHeader_2x = ".//*[@class='next-btn']";


    }
}
